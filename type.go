package vmigrate

import (
	"gitlab.com/stefarf/vdb"
)

type MigrationHandler struct {
	Name string
	Up   func(tx *vdb.Tx) error
	Down func(tx *vdb.Tx) error
}
