# README

- Run the `build.sh` script to compile the migrate source code to binary. The binary is saved in the `bin` directory.
- Run the `migrate.sh` script to run the migrate command. Check the available arguments:
    - `migrate.sh -h` to show all available arguments
    - `migrate.sh -up` to migrate up
    - `migrate.sh -up -steps n` to migrate up n steps
    - `migrate.sh -down` to migrate down
    - `migrate.sh -down -steps n` to migrate down n steps