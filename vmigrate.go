package vmigrate

import (
	"gitlab.com/stefarf/vdb"
)

func MustMigrate(db *vdb.DB, migrationHandlers []*MigrationHandler) {
	qMustCreateChangelogTable(db)

	if *fUp {
		mustMigrateUp(db, *fSteps, migrationHandlers)
	}

	if *fDown {
		mustMigrateDown(db, *fSteps, migrationHandlers)
	}
}
