module gitlab.com/stefarf/vmigrate

go 1.18

require (
	github.com/go-sql-driver/mysql v1.8.1
	github.com/jmoiron/sqlx v1.4.0
	gitlab.com/stefarf/vdb v0.3.7
)

require (
	filippo.io/edwards25519 v1.1.0 // indirect
	gitlab.com/stefarf/iferr v0.1.1 // indirect
)
