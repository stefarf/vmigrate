package vmigrate

import (
	"log"

	"gitlab.com/stefarf/vdb"
)

func mustMigrateDown(db *vdb.DB, steps int, migrationHandlers []*MigrationHandler) {
	migrationNameToHandler := map[string]*MigrationHandler{}
	for _, mig := range migrationHandlers {
		migrationNameToHandler[mig.Name] = mig
	}

	for _, migrationName := range qSelectLastMigrations(db.Db, steps) {
		mig := migrationNameToHandler[migrationName]
		runMigrateDownHandlerAndUpdateChangelog(db, migrationName, mig)
		log.Println("Migrate down:", migrationName)
	}
}

func runMigrateDownHandlerAndUpdateChangelog(db *vdb.DB, migrationName string, mig *MigrationHandler) {
	db.MustRunTransaction(func(tx *vdb.Tx) error {
		if mig != nil && mig.Down != nil {
			err := mig.Down(tx)
			if err != nil {
				return err
			}
		}
		txMustDeleteMigration(tx, migrationName)
		return nil
	})
}
