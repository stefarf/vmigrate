package vmigrate

import (
	"flag"
	"log"
)

var (
	fUp    = flag.Bool("up", false, "Run the migrate up.")
	fDown  = flag.Bool("down", false, "Run the migrate down.")
	fSteps = flag.Int("steps", -1, "Number of steps. 0 means unlimited steps.")
)

func init() {
	flag.Parse()

	if (*fUp && *fDown) || (!*fUp && !*fDown) {
		log.Fatal("must define either -up or -down")
	}
	if *fUp && *fSteps == -1 {
		*fSteps = 0 // default to unlimited steps
	}
	if *fDown && *fSteps == -1 {
		*fSteps = 1 // default to 1 step
	}
	if *fSteps < 0 {
		log.Fatal("-steps value must be 0 (unlimited steps) or positive integer")
	}
}
