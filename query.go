package vmigrate

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/stefarf/vdb"

	"gitlab.com/stefarf/vmigrate/internal/iferr"
)

func qMustCreateChangelogTable(db *vdb.DB) {
	db.MustExec(`
		CREATE TABLE IF NOT EXISTS changelog (
		    migration_name VARCHAR (100) NOT NULL,
		    done_at        DATETIME      NOT NULL DEFAULT NOW(),
		    UNIQUE (migration_name),
		    INDEX (done_at)
		) ENGINE = InnoDB `)
}

func qMustGetDoneMigrations(db *sqlx.DB) map[string]bool {
	done := map[string]bool{}

	rows, err := db.Query("SELECT migration_name FROM changelog")
	iferr.Panic(err)
	for rows.Next() {
		var migrationName string
		iferr.Panic(rows.Scan(&migrationName))
		done[migrationName] = true
	}
	return done
}

func qSelectLastMigrations(db *sqlx.DB, steps int) (migrationNames []string) {
	query := "SELECT migration_name FROM changelog ORDER BY migration_name DESC"
	var args []any
	if steps != 0 {
		query += " LIMIT ?"
		args = append(args, steps)
	}

	rows, err := db.Query(query, args...)
	iferr.Panic(err)
	for rows.Next() {
		var migrationName string
		iferr.Panic(rows.Scan(&migrationName))
		migrationNames = append(migrationNames, migrationName)
	}
	return
}

func txMustInsertMigration(tx *vdb.Tx, migrationName string) {
	tx.MustExec("INSERT INTO changelog(migration_name) VALUES (?)", migrationName)
}

func txMustDeleteMigration(tx *vdb.Tx, migrationName string) {
	tx.MustExec("DELETE FROM changelog WHERE migration_name = ?", migrationName)
}
