package vmigrate

import (
	"log"

	"gitlab.com/stefarf/vdb"
)

func mustMigrateUp(db *vdb.DB, steps int, migrationHandlers []*MigrationHandler) {
	unlimitedSteps := steps == 0

	done := qMustGetDoneMigrations(db.Db)
	for _, mig := range migrationHandlers {
		if !done[mig.Name] {
			mustRunMigrateUpHandlerAndUpdateChangelog(db, mig)
			log.Println("Migrate up:", mig.Name)

			done[mig.Name] = true
			if !unlimitedSteps {
				steps--
				if steps == 0 {
					return
				}
			}
		}
	}
}

func mustRunMigrateUpHandlerAndUpdateChangelog(db *vdb.DB, mig *MigrationHandler) {
	db.MustRunTransaction(func(tx *vdb.Tx) error {
		if mig != nil && mig.Up != nil {
			err := mig.Up(tx)
			if err != nil {
				return err
			}
		}
		txMustInsertMigration(tx, mig.Name)
		return nil
	})
}
