package main

import (
	"gitlab.com/stefarf/vdb"

	"gitlab.com/stefarf/vmigrate"
)

func main() {
	db := vdb.MustConnect(mysqlDriverAndDatasource())
	vmigrate.MustMigrate(db, migrationHandlers)
}
