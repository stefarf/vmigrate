package main

import (
	_ "embed"

	"gitlab.com/stefarf/vdb"
)

var (
	//go:embed data/0001.createTableHello.up.sql
	qUp0001CreateTable string

	//go:embed data/0001.createTableHello.down.sql
	qDown0001CreateTable string
)

func up0001CreateTable(tx *vdb.Tx) error {
	return tx.Exec(qUp0001CreateTable)
}

func down0001CreateTable(tx *vdb.Tx) error {
	return tx.Exec(qDown0001CreateTable)
}
