package main

import (
	"gitlab.com/stefarf/vmigrate"
)

var migrationHandlers = []*vmigrate.MigrationHandler{
	{Name: "0001-create-table", Up: up0001CreateTable, Down: down0001CreateTable},
	{Name: "0002-insert-data", Up: up0002InsertData, Down: down0002InsertData},
}
