package main

import (
	_ "embed"

	"gitlab.com/stefarf/vdb"
)

var (
	//go:embed data/0002.insertData.up.sql
	qUp0002InsertData string

	//go:embed data/0002.insertData.down.sql
	qDown0002InsertData string
)

func up0002InsertData(tx *vdb.Tx) error {
	return tx.Exec(qUp0002InsertData)
}

func down0002InsertData(tx *vdb.Tx) error {
	return tx.Exec(qDown0002InsertData)
}
