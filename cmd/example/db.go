package main

import (
	"time"

	"github.com/go-sql-driver/mysql"
)

func mysqlDriverAndDatasource() (string, string) {
	dbCfg := mysql.Config{
		Net:                  "tcp",
		Addr:                 "localhost:3306",
		DBName:               "learn",
		User:                 "root",
		Passwd:               "secret",
		Loc:                  getLocation("Asia/Jakarta"),
		AllowNativePasswords: true,
		MultiStatements:      true,
		ParseTime:            true,
	}
	return "mysql", dbCfg.FormatDSN()
}

func getLocation(name string) *time.Location {
	loc, _ := time.LoadLocation(name)
	return loc
}
