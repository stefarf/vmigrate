#!/usr/bin/env bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
cd $DIR/..
REPO=$PWD

cd $REPO/cmd/example
go build -o $REPO/bin/migrate
