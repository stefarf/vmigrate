#!/usr/bin/env bash
DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" >/dev/null && pwd)"
cd $DIR/..
REPO=$PWD

mkdir -p $REPO/data

docker stop mariadb
docker rm mariadb
docker run \
  --detach \
  --name mariadb \
  --env TZ="Asia/Jakarta" \
  --env MARIADB_ROOT_PASSWORD="secret" \
  -v $REPO/data:/var/lib/mysql \
  -p 3306:3306 \
  mariadb:latest
