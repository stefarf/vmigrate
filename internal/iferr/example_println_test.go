package iferr_test

import (
	"errors"
	"gitlab.com/stefarf/vmigrate/internal/iferr"
)

var err = errors.New("this is error")

func ExamplePrintln() {
	iferr.Println(err)
}
