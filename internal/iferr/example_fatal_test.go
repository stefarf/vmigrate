package iferr_test

import (
	"errors"
	"gitlab.com/stefarf/vmigrate/internal/iferr"
)

var errFatal = errors.New("this is fatal error")

func ExampleFatal() {
	iferr.Fatal(errFatal)
}
