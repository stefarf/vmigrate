package iferr

import (
	"log"
)

// Panic checks if `err` is not nil, then it will panic.
func Panic(err error) {
	if err != nil {
		panic(err)
	}
}

// Fatal checks if `err` is not nil, then it will run the `log.Fatal(err)`.
func Fatal(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// Println checks if `err` is not nil, then it will print the err.
func Println(err error) {
	if err != nil {
		log.Println(err)
	}
}
