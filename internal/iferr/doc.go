/*
Package iferr provides helper to simplify error handling.

So if you find that you often write this code:

	err := someFunction()
	if err != nil {
		panic(err)
	}

then you can simplify the above code with:

	err := someFunction()
	iferr.Panic(err)

It will surely save time and lines.
*/
package iferr
